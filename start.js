function NewGame()
{
    pacman.posX = 9;
    pacman.posY = 18;
    pacman.score = 0;
    pacman.facingDir = null;
    ButtonClass.style.display = "none";
    CreateMap();
    mapDynamic[pacman.posY][pacman.posX] = 2;
    var index = (pacman.posY * 19) + pacman.posX;
    var pacmanNow = document.getElementsByClassName("box")[index];
    pacmanNow.classList.remove(pacmanNow.classList.item([pacmanNow.classList.length-1]));
    pacmanNow.classList.add("pacman_class");
    setInterval(move,140);
}